﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlanetSettingsDTO", menuName = "Planet settings")]
[System.Serializable]
public class PlanetSettings : ScriptableObject
{
    public bool shootingPrevented = false;
    public float shootDelay = 2;
    public float health = 2;
    public float velocity = .14f;
    public float orbitRadius = 2.8f;
    public float angle = 0;
    public float scale = .7f;
    public int rocketPoolSize = 4;
    public bool isMotherland = false;
}
