﻿using UnityEngine;

[CreateAssetMenu(fileName = "RocketSettingsDTO", menuName = "Rocket settings")]
[System.Serializable]
public class RocketSettings : ScriptableObject
{
    public float velocity = .14f;
    public float angle = 0;
    public float size = .7f;
    public float damage = 10;
    public int lifeTime = 14;
}
