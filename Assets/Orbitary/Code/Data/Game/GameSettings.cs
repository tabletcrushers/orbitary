﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameSettingsDTO", menuName = "Game settings")]
[System.Serializable]
public class GameSettings : ScriptableObject
{
	public GameObject planetPrefab;
	[Range(1, 21)]
	public int planetsAmount;
}
