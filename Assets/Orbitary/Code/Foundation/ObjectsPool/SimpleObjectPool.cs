﻿using System;
using System.Collections.Generic;

public class SimpleObjectPool<T> : IObjectPool<T>
{
    public Action<T> ResetObjectAction { get; set; }

    public Action EmptyPoolAction { get; set; }

    private readonly List<T> availableObjects;
    protected readonly List<T> objectsInUse;

    public SimpleObjectPool(Action<T> resetObjectAction = null)
    {
        availableObjects = new List<T>();
        objectsInUse = new List<T>();
        ResetObjectAction = resetObjectAction;
        EmptyPoolAction = () => throw new Exception($"{availableObjects} is empty");
    }

    public IObjectPool<T> Add(T obj)
    {
        availableObjects.Add(obj);
        return this;
    }

    public T Get()
    {
        if (availableObjects.Count == 0)
        {
            EmptyPoolHandler();
        }

        T obj = availableObjects[0];
        objectsInUse.Add(obj);
        availableObjects.Remove(obj);

        return obj;
    }

    public IObjectPool<T> Return(T obj)
    {
        if (!objectsInUse.Remove(obj))
        {
            throw new Exception($"There is no {obj} in use");
        }

        ResetObjectAction?.Invoke(obj);
        availableObjects.Add(obj);

        return this;
    }
    public List<T> ObjectsInUseReport()
    {
        return objectsInUse;
    }

    public List<T> ObjectsInPoolReport()
    {
        return availableObjects;
    }

    protected virtual void EmptyPoolHandler()
    {
        EmptyPoolAction?.Invoke();
    }
}