﻿using System;

public class AutoReturnObjectPool<T> : SimpleObjectPool<T>
{
    public Predicate<T> ObjectBackToPoolPredicate { get; set; }

    public AutoReturnObjectPool(Predicate<T> objectBackToPoolPredicate, Action<T> ResetObjectAction = null) : base(ResetObjectAction)
    {
        ObjectBackToPoolPredicate = objectBackToPoolPredicate;
    }

    public void TryToReturn()
    {
        foreach (T obj in objectsInUse.FindAll(ObjectBackToPoolPredicate))
        {
            Return(obj);
        }
    }
}