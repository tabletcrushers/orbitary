﻿using System;
using System.Collections.Generic;
public interface IObjectPool<T>
{
    Action<T> ResetObjectAction { get; set; }
    Action EmptyPoolAction { get; set; }
    IObjectPool<T> Add(T obj);
    T Get();
    IObjectPool<T> Return(T obj);
    List<T> ObjectsInUseReport();
}