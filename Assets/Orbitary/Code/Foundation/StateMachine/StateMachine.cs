﻿public class StateMachine : IUpdatable
{
    private IState currentState;
    private readonly IState[] states;

    public StateMachine(IState[] states)
    {
        this.states = states;
        SetStateByName(states[0].Name);
    }

    public void SwitchState(string name)
    {
        SetStateByName(name);
    }

    public void Update()
    {
        currentState.Update();
        TryToSwitchState();
    }

    private void TryToSwitchState()
    {
        if (currentState.LifeState == LifeCycleState.Stopped && currentState.TryToGetExitTransition(out IExitFromStateTransition exitTransition))
        {
            currentState.LifeState = LifeCycleState.Stopped;
            currentState.Stop();
            SetStateByName(exitTransition.DestinationStateName);
        }
    }

    private void SetStateByName(string stateName)
    {
        foreach (IState state in states)
        {
            if (state.Name == stateName)
            {
                if (currentState != null)
                {
                    currentState.LifeState = LifeCycleState.Stopped;
                    currentState.Stop();
                }
                
                currentState = state;
                currentState.LifeState = LifeCycleState.Started;
                currentState.Start();

                return;
            }
        }
    }
}
