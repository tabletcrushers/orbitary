﻿public class State : IState
{
    public string Name { get; }
    public LifeCycleState LifeState { get; set; }

    private IPluginsContainer plugins;
    private readonly IExitFromStateTransition[] exitTransitions;

    public State(string name, IPluginsContainer plugins, IExitFromStateTransition[] exitTransitions = null)
    {
        Name = name;
        this.exitTransitions = exitTransitions;
        this.plugins = plugins;
    }

    public void Update()
    {
        if (LifeState == LifeCycleState.Started)
        {
            if (plugins.LifeState == LifeCycleState.Stopped)
            {
                LifeState = LifeCycleState.Stopped;
            }
            else
            {
                plugins.Update();
            }
        }
    }

    public void Start()
    {
        plugins.Start();
    }

    public void Stop()
    {
        plugins.Stop();
    }

    public bool TryToGetExitTransition(out IExitFromStateTransition transition)
    {
        transition = null;

        if (exitTransitions == null || exitTransitions.Length == 0)
        {
            return false;
        }


        foreach (IExitFromStateTransition t in exitTransitions)
        {
            if (t.Evaluate())
            {
                transition = t;

                return true;
            }
        }
        
        return false;
    }
}
