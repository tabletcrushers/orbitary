﻿public class ExitFromStateTransition: IExitFromStateTransition
{
    private readonly IEvaluable[] predicates;
    public string DestinationStateName { get; }

    public ExitFromStateTransition(string destinationStateName, IEvaluable[] predicates)
    {
        this.predicates = predicates;
        DestinationStateName = destinationStateName;
    }

    public bool Evaluate(object obj)
    {
        foreach(IEvaluable predicate in predicates)
        {
            if(!predicate.Evaluate(obj)) return false;
        }

        return true;
    }
}
