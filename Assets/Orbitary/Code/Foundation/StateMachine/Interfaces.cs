﻿public interface IState : IUpdatable, ILifeCycle, INamed
{
    bool TryToGetExitTransition(out IExitFromStateTransition transition);
}

public interface IExitFromStateTransition : IEvaluable
{
    string DestinationStateName { get; }
}
