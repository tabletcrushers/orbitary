﻿public abstract class AbstractPredicate : IPredicate
{
    public abstract bool Evaluate(object obj = null);

    public virtual void Reset()
    {

    }
}
