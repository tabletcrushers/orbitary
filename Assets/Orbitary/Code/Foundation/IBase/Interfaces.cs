﻿public interface IDisposable
{
    void Dispose();
}

public interface IEvaluable
{
    bool Evaluate(object o = null);
}

public interface ILifeCycle
{
    LifeCycleState LifeState { get; set; }
    void Start();
    void Stop();
}

public interface INamed
{
    string Name { get; }
}

public interface IPriority
{
    bool TopPriority { get; }
}

public interface IUpdatable
{
    void Update();
}

public interface IResetable
{
    void Reset();
}

public interface IPredicate : IEvaluable, IResetable
{

}

public enum LifeCycleState
{
    Stopped,
    Started
}