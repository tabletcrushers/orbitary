﻿using System.Collections.Generic;

public class PluginsContainer : IPluginsContainer
{
    public PluginResponsibilities Responsibility { get; set; }
    public LifeCycleState LifeState { get; set; }

    readonly protected List<IPluginsContainer> plugins;

    protected PluginsContainer()
    {
        Responsibility = PluginResponsibilities.Regular;
        LifeState = LifeCycleState.Stopped;
        plugins = new List<IPluginsContainer>();
    }

    public virtual void Start()
    {
        LifeState = LifeCycleState.Started;
    }

    public virtual void Stop()
    {
        foreach(IPluginsContainer plugin in plugins)
        {
            plugin.Stop();
        }

        LifeState = LifeCycleState.Stopped;
    }

    public virtual IPluginsContainer AddPlugin(IPluginsContainer plugin)
    {
        plugins.Add(plugin);

        return (IPluginsContainer)this;
    }

    public virtual IPluginsContainer RemovePlugin(IPluginsContainer plugin)
    {
        plugins.Remove(plugin);

        return (IPluginsContainer)this;
    }

    public virtual void Update()
    {
    }
}
