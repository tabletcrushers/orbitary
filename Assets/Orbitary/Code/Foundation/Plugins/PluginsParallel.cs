﻿using UnityEngine;

public class PluginsParallel : PluginsContainer
{
    public override void Update()
    {
        if (LifeState == LifeCycleState.Stopped)
        {
            return;
        }

        LifeState = LifeCycleState.Stopped;

        foreach (IPluginsContainer plugin in plugins)
        {
            if (plugin.LifeState == LifeCycleState.Stopped)
            {
                if (plugin.Responsibility == PluginResponsibilities.Parent)
                {
                    Stop();

                    return;
                }
                
                continue;
            }

            LifeState = LifeCycleState.Started;

            plugin.Update();
        }
    }

    public override void Start()
    {
        base.Start();

        foreach (IPluginsContainer plugin in plugins)
        {
            plugin.Start();
        }
    }
}
