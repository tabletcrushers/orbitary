﻿public class PluginsSequence : PluginsContainer
{
    private IPluginsContainer currentPlugin;

    public override void Update()
    {
        if (LifeState == LifeCycleState.Stopped)
        {
            return;
        }

        if (currentPlugin.LifeState == LifeCycleState.Stopped)
        {
            if (currentPlugin.Responsibility == PluginResponsibilities.Parent)
            {
                Stop();

                return;
            }

            int nextIndex = plugins.IndexOf(currentPlugin) + 1;

            if (nextIndex >= plugins.Count)
            {
                Stop();

                return;
            }

            currentPlugin = plugins[nextIndex];
            currentPlugin.Start();
        }

        currentPlugin.Update();
    }

    public override void Start()
    {
        if(plugins.Count == 0)
        {
            throw new System.Exception("Add at least one plugin into sequence before using");
        }
        base.Start();
        currentPlugin = plugins[0];
        currentPlugin.Start();
    }

    public override void Stop()
    {
        if (LifeState == LifeCycleState.Stopped)
        {
            return;
        }
        base.Stop();
        currentPlugin.Stop();
    }
}
