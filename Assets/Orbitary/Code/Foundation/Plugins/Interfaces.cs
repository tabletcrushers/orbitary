﻿public interface IPluginsContainer : IUpdatable, ILifeCycle
{
    PluginResponsibilities Responsibility { get; set; }
    IPluginsContainer AddPlugin(IPluginsContainer plugin);
    IPluginsContainer RemovePlugin(IPluginsContainer plugin);
}
