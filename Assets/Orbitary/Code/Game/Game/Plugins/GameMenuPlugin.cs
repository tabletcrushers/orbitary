﻿public class GameMenuPlugin : GuiHolderPlugin
{
    public override void Start()
    {
        base.Start();

        gameplayMenu.SetActive(true);
        mainMenu.SetActive(false);

        Stop();
    }
}
