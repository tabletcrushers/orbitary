﻿using UnityEngine;

public class ResumeRocketHostsPlugin : GameHostHolderPlugin
{
    public ResumeRocketHostsPlugin(GameHost host) : base(host) { }

    public override void Start()
    {
        base.Start();
        foreach (GameObject rocket in host.GetActiveRockets())
        {
            rocket.GetComponent<RocketHost>().rocketPlugins.Start();
        }
        Stop();
    }
}