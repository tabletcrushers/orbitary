﻿using UnityEngine;

public class GuiHolderPlugin : PluginsContainer
{
    protected GameObject mainMenu;
    protected GameObject gameplayMenu;
    protected GameObject buttonPause;
    protected GameObject buttonResume;

    public GuiHolderPlugin()
    {
        buttonPause = GameObject.Find("GameMenu/Pause");
        buttonResume = GameObject.Find("GameMenu/Resume");
        mainMenu = GameObject.Find("MainMenu");
        gameplayMenu = GameObject.Find("GameMenu");
    }
}
