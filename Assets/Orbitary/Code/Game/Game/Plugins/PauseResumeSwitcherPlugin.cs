﻿using UnityEngine;

public class PauseResumeSwitcherPlugin :  PluginsContainer
{
    private GameObject buttonPause;
    private GameObject buttonResume;
    public PauseResumeSwitcherPlugin()
    {
        buttonPause = GameObject.Find("GameMenu/Pause");
        buttonResume = GameObject.Find("GameMenu/Resume");
    }
    public override void Start()
    {
        base.Start();

        if(buttonPause.activeSelf)
        {
            buttonResume.SetActive(true);
            buttonPause.SetActive(false);
        }
        else
        {
            buttonResume.SetActive(false);
            buttonPause.SetActive(true);
        }
        
        Stop();
    }
}
