﻿using System.Collections.Generic;
using UnityEngine;
public interface IRocketPlanetInteractionSubPlugin
{
    void Interact(GameObject rocket, PlanetHost planet, List<PlanetHost> planets);
}