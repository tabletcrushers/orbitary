﻿using System.Collections.Generic;
using UnityEngine;
public class AttractionSubPlugin : IRocketPlanetInteractionSubPlugin
{
    private float attractionMultiplayer;
    public void Interact(GameObject rocket, PlanetHost planet, List<PlanetHost> planets)
    {
        attractionMultiplayer = 87 / planets.Count;
        PlanetSettings planetSettings = planet.gameObject.GetComponent<PlanetHost>().settings;
        Vector3 targetPosition = planet.transform.position;
        FakeAtrractionWithRotation(rocket.transform, targetPosition, planetSettings.scale);
    }

    private void FakeAtrractionWithRotation(Transform rocketTransform, Vector3 planetPosition, float size = 1)
    {
        Vector2 inversedPosition = rocketTransform.InverseTransformPoint(planetPosition);
        float angle = Vector2.Angle(Vector2.right, inversedPosition);

        angle = inversedPosition.y < 0 ? -angle : angle;

        float attractingMagnitude = size * attractionMultiplayer;
        rocketTransform.Rotate(Vector3.forward, attractingMagnitude * Time.deltaTime * Mathf.Sign(angle));
    }
}
