﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyAIPlugin : PlanetHostHolderPlugin
{
	private float velocity = 0;
	public EnemyAIPlugin (PlanetHost host) : base(host)
	{
		while(Mathf.Abs(velocity) < .28f)
		{
			velocity = Random.Range(-.5f, .5f);
		}
	}

	public override void Update()
	{
		base.Update();

		if(Random.value < .004f) host.Fire();

		host.gameObject.transform.Rotate(new Vector3(0, 0, velocity));
	}
}
