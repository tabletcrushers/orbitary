﻿using System.Collections.Generic;
using UnityEngine;

public class CollisionsCheckerSubPlugin : GameHostHolderPlugin, IRocketPlanetInteractionSubPlugin
{
    private readonly float rocketLength = .2f;

    public CollisionsCheckerSubPlugin(GameHost host) : base(host) { }
    public void Interact(GameObject rocket, PlanetHost planet, List<PlanetHost> planets)
    {
        PlanetSettings planetSettings = planet.gameObject.GetComponent<PlanetHost>().settings;
        RocketSettings rocketSettings = rocket.gameObject.GetComponent<RocketHost>().settings;
        Vector3 targetPosition = planet.transform.position;

        if (CheckCollision(rocket.transform.position, planet.transform.position, planetSettings.scale / 2 + rocketLength / 1.2f))
        {
            rocket.SetActive(false);
            planetSettings.health -= rocketSettings.damage;
            planet.life.transform.localScale = new Vector3(planetSettings.health, 1, 1);
            if (planetSettings.health <= 0)
            {
                if (planetSettings.isMotherland || planets.Count == 2)
                {
                    host.OnMainMenuButtonClick();
                }
                else
                {
                    host.planets.Remove(planet);
                    GameObject.Destroy(planet.life);
                    GameObject.Destroy(planet.gameObject);
                }
            }
        }
    }

    bool CheckCollision(Vector3 rocketPosition, Vector3 planetPosition, float planetSize = 1)
    {
        if (Vector3.Distance(rocketPosition, planetPosition) <= planetSize * .7f)
        {
            return true;
        }

        return false;
    }
}