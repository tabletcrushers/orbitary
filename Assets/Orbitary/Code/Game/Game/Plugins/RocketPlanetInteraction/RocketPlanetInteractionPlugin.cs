﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
public class RocketPlanetInteractionPlugin : GameHostHolderPlugin
{
    private List<IRocketPlanetInteractionSubPlugin> subPlugins;
    public RocketPlanetInteractionPlugin(GameHost host) : base(host) 
    {
        subPlugins = new List<IRocketPlanetInteractionSubPlugin>();
    }

    public RocketPlanetInteractionPlugin AddSubPlugin(IRocketPlanetInteractionSubPlugin subPlugin)
    {
        subPlugins.Add(subPlugin);
        return this;
    }

    public override void Update()
    {
        List<PlanetHost> planets = host.GetPlanets();
        List<PlanetHost> deadPlanets = planets.FindAll(x => x == null);

        planets = planets.Except(deadPlanets).ToList();

        foreach (GameObject rocket in host.GetActiveRockets())
        {
            RocketSettings rocketSettings = rocket.gameObject.GetComponent<RocketHost>().settings;

            foreach (PlanetHost planet in planets)
            {
                foreach (IRocketPlanetInteractionSubPlugin subPlugin in subPlugins)
                {
                    subPlugin.Interact(rocket, planet, planets);
                }
            }
        }
    }
}