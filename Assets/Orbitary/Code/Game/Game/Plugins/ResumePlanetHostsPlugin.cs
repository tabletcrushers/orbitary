﻿public class ResumePlanetHostsPlugin : GameHostHolderPlugin
{
    public ResumePlanetHostsPlugin(GameHost host) : base(host) { }

    public override void Start()
    {
        base.Start();
        foreach (PlanetHost planet in host.planets)
        {
            planet.planetPlugins.Start();
        }
        Stop();
    }
}