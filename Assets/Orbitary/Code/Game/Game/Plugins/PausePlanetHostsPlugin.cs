﻿public class PausePlanetHostsPlugin : GameHostHolderPlugin
{
    public PausePlanetHostsPlugin(GameHost host) : base(host) { }

    public override void Start()
    {
        base.Start();
        foreach(PlanetHost planet in host.planets)
        {
            planet.planetPlugins.Stop();
        }
        Stop();
    }
}
