﻿using UnityEngine;

public class PauseRocketHostsPlugin : GameHostHolderPlugin
{
    public PauseRocketHostsPlugin(GameHost host) : base(host) { }

    public override void Start()
    {
        base.Start();
        foreach (GameObject rocket in host.GetActiveRockets())
        {
            rocket.GetComponent<RocketHost>().rocketPlugins.Stop();
        }
        Stop();
    }
}
