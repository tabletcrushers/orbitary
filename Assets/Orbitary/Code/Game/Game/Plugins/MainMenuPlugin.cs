﻿using UnityEngine;

public class MainMenuPlugin : GuiHolderPlugin
{
    public override void Start()
    {
        base.Start();

        gameplayMenu.SetActive(false);
        mainMenu.SetActive(true);
        buttonResume.SetActive(false);
        buttonPause.SetActive(true);

        Stop();
    }
}
