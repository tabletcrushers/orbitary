﻿using UnityEngine;

public class SpaceBuilderPlugin : GameHostHolderPlugin
{
    public SpaceBuilderPlugin(GameHost host) : base(host) { }

    public override void Start()
    {
        base.Start();

        host.planets = host.GetComponent<SpaceBuilder>().Build(host.planetPrefab, host.gameSettings.planetsAmount, host.planetSettingsTemplate);
        //host.planets.Add(GameObject.Find("Sun").GetComponent<PlanetHost>());

        Stop();
    }

}
