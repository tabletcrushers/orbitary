﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using UnityEngine;

public class GameHost : MonoBehaviour
{
    public GameSettings gameSettings;
    public GameObject planetPrefab;
    public PlanetSettings planetSettingsTemplate;
    public List<PlanetHost> planets;
    public bool isPaused = false;

    public static readonly string INITIAL_STATE = "InitialState";
    public static readonly string MAIN_MENU_STATE = "MainMenuState";
    public static readonly string GAMEPLAY_STATE = "GameplayState";
    public static readonly string PAUSE_STATE = "PauseState";
    public static readonly string RESUME_STATE = "ResumeState";

    private StateMachine stateMachine;

    private void Start()
    {
        InitStateMachine();
    }

    private void InitStateMachine()
    {
        AllwaysTruePredicate truePredicate = new AllwaysTruePredicate();
        ExitFromStateTransition toGameplayTransition = new ExitFromStateTransition(GAMEPLAY_STATE, new IEvaluable[] { truePredicate });

        IPluginsContainer pauseResumeSwitcherPlugin = new PauseResumeSwitcherPlugin();

        // Initial state
        IPluginsContainer initGamePlugin = new SpaceBuilderPlugin(this);

        IState initialState = new State(INITIAL_STATE, initGamePlugin, new IExitFromStateTransition[] { toGameplayTransition });

        // Gameplay state
        IPluginsContainer gameplayPlugins = new PluginsSequence();
        IPluginsContainer gameplayParallelPlugins = new PluginsParallel();

        RocketPlanetInteractionPlugin rocketPlanetInteractionPlugin = new RocketPlanetInteractionPlugin(this);
        AttractionSubPlugin attractionSubPlugin = new AttractionSubPlugin();
        CollisionsCheckerSubPlugin collisionsCheckerSubPlugin = new CollisionsCheckerSubPlugin(this);
        GameMenuPlugin gameMenuPlugin = new GameMenuPlugin();

        rocketPlanetInteractionPlugin
            .AddSubPlugin(attractionSubPlugin)
            .AddSubPlugin(collisionsCheckerSubPlugin);
        
        gameplayParallelPlugins
            .AddPlugin(rocketPlanetInteractionPlugin);
        
        gameplayPlugins
            .AddPlugin(gameMenuPlugin)
            .AddPlugin(gameplayParallelPlugins);

        IState gameplayState = new State(GAMEPLAY_STATE, gameplayPlugins);

        // Pause state
        IPluginsContainer pausePlugins = new PluginsSequence();

        IPluginsContainer pausePlanetHostsPlugin = new PausePlanetHostsPlugin(this);
        IPluginsContainer pauseRocketHostsPlugin = new PauseRocketHostsPlugin(this);

        pausePlugins
            .AddPlugin(pausePlanetHostsPlugin)
            .AddPlugin(pauseRocketHostsPlugin)
            .AddPlugin(pauseResumeSwitcherPlugin);
        
        IState pauseState = new State(PAUSE_STATE, pausePlugins);

        // Resume state
        IPluginsContainer resumePlugins = new PluginsSequence();

        IPluginsContainer resumePlanetHostsPlugin = new ResumePlanetHostsPlugin(this);
        IPluginsContainer resumeRocketHostsPlugin = new ResumeRocketHostsPlugin(this);

        resumePlugins
            .AddPlugin(pauseResumeSwitcherPlugin)
            .AddPlugin(resumeRocketHostsPlugin)
            .AddPlugin(resumePlanetHostsPlugin);

        IState resumeState = new State(RESUME_STATE, resumePlugins, new IExitFromStateTransition[] { toGameplayTransition });

        // Main menu state
        IPluginsContainer mainMenuPlugins = new PluginsSequence();

        MainMenuPlugin mainMenuPlugin = new MainMenuPlugin();

        mainMenuPlugins.AddPlugin(mainMenuPlugin);

        IState mainMenuState = new State(MAIN_MENU_STATE, mainMenuPlugins);

        stateMachine = new StateMachine(new IState[] { mainMenuState, initialState, gameplayState, pauseState, resumeState });
    }

    public List<GameObject> GetActiveRockets()
    {
        List<GameObject> rockets = new List<GameObject>();
        foreach (PlanetHost planet in planets)
        {
            rockets.AddRange(planet.GetActiveRockets());
        }

        return rockets;
    }

    public List<PlanetHost> GetPlanets()
    {
        return planets;
    }

    private void Clear()
    {
        foreach (PlanetHost planet in planets)
        {
            if (planet.settings.health > 1) continue;

            foreach (GameObject rocketGameObject in planet.rockets.ObjectsInPoolReport())
            {
                GameObject.Destroy(rocketGameObject);
            }
            foreach (GameObject rocketGameObject in planet.rockets.ObjectsInUseReport())
            {
                GameObject.Destroy(rocketGameObject);
            }

            if (planet == null) continue;

            GameObject.Destroy(planet.gameObject);
            GameObject.Destroy(planet.life);
        }

        
    }

    public void Restart()
    {
        Clear();
        Start();
    }

    public void OnStartButtonClick()
    {
        stateMachine.SwitchState(INITIAL_STATE);
    }
    public void OnPauseButtonClick()
    {
        stateMachine.SwitchState(PAUSE_STATE);
    }
    public void OnResumeButtonClick()
    {
        stateMachine.SwitchState(RESUME_STATE);
    }

    public void OnMainMenuButtonClick()
    {
        Clear();
        stateMachine.SwitchState(MAIN_MENU_STATE);
    }

    public void OnSaveButtonClick()
    {
        /* BinaryFormatter formater = new BinaryFormatter();

         using (FileStream fileToSave = new FileStream("game.bin", FileMode.Create))
         {
             formater.Serialize(fileToSave)
         }*/
    }

    public void OnLoadButtonClick()
    {

    }

    void Update()
    {
        stateMachine.Update();
    }
}

