﻿using System.Collections.Generic;
using UnityEngine;

public class PlanetHost : MonoBehaviour
{
    public PlanetSettings settings;
    public RocketSettings[] rocketSettingsTemplate;
    public GameObject rocketPrefab;
    public AutoReturnObjectPool<GameObject> rockets;
    public IPluginsContainer planetPlugins;
    public GameObject life;

    private GameObject BuildRocket()
    {
        if (rocketSettingsTemplate != null)
        {
            GameObject rocket = Instantiate(rocketPrefab);
            RocketSettings rocketSettings = Instantiate(rocketSettingsTemplate[Random.Range(0, 3)]);
            rocket.GetComponent<RocketHost>().settings = rocketSettings;
            rocket.SetActive(false);
            rockets.Add(rocket);

            return rocket;
        }

        return null;
    }
    public void Start()
    {
        rockets = new AutoReturnObjectPool<GameObject>((GameObject r) => !r.activeSelf, (GameObject r) => r.GetComponent<RocketHost>().StopAllCoroutines());
        for (int i = 0; i < settings.rocketPoolSize; i++)
        {
            BuildRocket();
        }
        rockets.EmptyPoolAction = () => BuildRocket();

        life = Instantiate(life);

        planetPlugins = new PluginsParallel();
        IPluginsContainer movementPlugin = new MovementPlugin(this);
        IPluginsContainer tryToReturnRocketIntoPoolPlugin = new TryToReturnRocketIntoPoolPlugin(rockets);
        EnemyAIPlugin enemyAIPlugin = new EnemyAIPlugin(this);

        planetPlugins
            .AddPlugin(movementPlugin)
            .AddPlugin(tryToReturnRocketIntoPoolPlugin);

        if (settings.isMotherland)
        {
            MotherandSetup();
        }
        else if(settings.health <= 1)
        {
            planetPlugins.AddPlugin(enemyAIPlugin);
        }

        planetPlugins.Start();
    }

    public void Fire()
    {
        if (settings.shootingPrevented)
        {
            return;
        }

        GameObject rocket = rockets.Get();
        rocket.transform.rotation = gameObject.transform.rotation;
        float angle = gameObject.transform.eulerAngles.z * Mathf.Deg2Rad;
        Vector3 originOffset = new Vector3(Mathf.Cos(angle) * settings.scale, Mathf.Sin(angle) * settings.scale);
        rocket.transform.position = gameObject.transform.position + originOffset;
        rocket.SetActive(true);
        rocket.GetComponent<RocketHost>().StartCoroutine(Actuate.Delay(21, () => rocket.SetActive(false)));
        settings.shootingPrevented = true;
        StartCoroutine(Actuate.Delay(settings.shootDelay, () => settings.shootingPrevented = false));
    }

    public void Rotate(float deltaRotation)
    {
        gameObject.transform.Rotate(new Vector3(0, 0, deltaRotation));
    }

    public List<GameObject> GetActiveRockets()
    {
        return rockets.ObjectsInUseReport();
    }

    public void Update()
    {
        planetPlugins.Update();
        life.transform.position = gameObject.transform.position + new Vector3(0, .5f, 0);
    }

    private void MotherandSetup()
    {
        IPluginsContainer playerInputPlugin = new PlayerFirePlugin(this);
        planetPlugins.AddPlugin(playerInputPlugin);
    }
}
