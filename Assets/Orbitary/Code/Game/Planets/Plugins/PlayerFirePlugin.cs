﻿using UnityEngine;

public class PlayerFirePlugin : PlanetHostHolderPlugin
{
    private bool rotationAllowed = false;
    public PlayerFirePlugin(PlanetHost host) : base(host) { }
    public override void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            host.Fire();
        }
        if (Input.GetMouseButtonDown(1))
        {
            rotationAllowed = true;
        }
        if (Input.GetMouseButtonUp(1))
        {
            rotationAllowed = false;
        }

        if (Input.GetAxis("Mouse X") < 0)
        {
            if (!rotationAllowed) return;
            host.Rotate(-1);
        }
        if (Input.GetAxis("Mouse X") > 0)
        {
            if (!rotationAllowed) return;
            host.Rotate(1);
        }
    }
}
