﻿using UnityEngine;

public class MovementPlugin : PlanetHostHolderPlugin
{
	private readonly Vector2 galaxyCentre = new Vector2();
	public MovementPlugin(PlanetHost host) : base(host) { }
	public override void Update()
	{
		host.settings.angle += host.settings.velocity * Time.deltaTime;
		var offset = new Vector2(Mathf.Sin(host.settings.angle), Mathf.Cos(host.settings.angle)) * host.settings.orbitRadius;
		host.gameObject.transform.position = galaxyCentre + offset;
	}
}
