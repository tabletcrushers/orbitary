﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TryToReturnRocketIntoPoolPlugin : PluginsContainer
{
	AutoReturnObjectPool<GameObject> rocketPool;
	public TryToReturnRocketIntoPoolPlugin(AutoReturnObjectPool<GameObject> rocketPool)
	{
		this.rocketPool = rocketPool;
	}

	public override void Update()
	{
		base.Update();
		rocketPool.TryToReturn();
	}
}
