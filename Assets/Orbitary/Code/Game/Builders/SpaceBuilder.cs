﻿using System.Collections.Generic;
using UnityEngine;

public class SpaceBuilder : MonoBehaviour
{
    //public GameSettings gameSettings;
    //public PlanetSettings planetSettingsTemplate;

    private readonly float sunScale = 1;

    public List<PlanetHost> Build(GameObject planetPrefab, int planetsAmount, PlanetSettings planetSettingsTemplate)
    {
        List<PlanetHost> planets = new List<PlanetHost>();
        float radius = 2;
        float[] radiuses = new float [planetsAmount];
        for (int i = 0; i < planetsAmount; i++)
        {
            radiuses[i] = radius;
            radius += 1;
        }
        ShuffleList(radiuses);

        float angleStep = Mathf.PI * 4 / planetsAmount;
        float angle = 0;

        for (int i = 0; i < planetsAmount; i++)
        {
            GameObject planet = Instantiate(planetPrefab);
            
            PlanetSettings planetSettings = Instantiate(planetSettingsTemplate);

            planetSettings.health = 1;
            planetSettings.angle = angle;
            planetSettings.orbitRadius = radiuses[i];
            planetSettings.velocity = .2f / radiuses[i] * 4; 
            float s = planetSettings.scale = sunScale / Random.Range(1.7f, 2.8f);
            planet.transform.localScale = new Vector3(s, s, s);
            if (i == 0)
            {
                planetSettings.isMotherland = true;
                planet.GetComponent<Renderer>().material.color = Color.green;
            }
            else
            {
                planetSettings.shootDelay = Random.Range(7, 14);
            }

            PlanetHost planetHost = planet.GetComponent<PlanetHost>();
            planetHost.settings = planetSettings;

            planets.Add(planet.GetComponent<PlanetHost>());
            
            angle += angleStep;
        }

        return planets;
    }

    private void ShuffleList <T>(T[] arr)
    {
        System.Random r = new System.Random();

        int n = arr.Length;
        while (n > 1)
        {
            n--;
            int k = r.Next(n + 1);
            T value = arr[k];
            arr[k] = arr[n];
            arr[n] = value;
        }
    }
}
