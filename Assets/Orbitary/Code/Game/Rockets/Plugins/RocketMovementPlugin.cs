﻿using UnityEngine;

public class RocketMovementPlugin : RocketHostHolderPlugin
{
    public RocketMovementPlugin(RocketHost host) : base(host) { }
    public override void Update()
    {
        host.gameObject.transform.Translate(new Vector3(host.settings.velocity * Time.deltaTime / 4, 0));
    }
}
