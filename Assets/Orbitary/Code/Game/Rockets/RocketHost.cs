﻿using UnityEngine;

public class RocketHost : MonoBehaviour
{
    public RocketSettings settings;
    public IPluginsContainer rocketPlugins;

    public void Start()
    {
        rocketPlugins = new PluginsParallel();
        RocketMovementPlugin rocketMovementPlugin = new RocketMovementPlugin(this);
        rocketPlugins.AddPlugin(rocketMovementPlugin);
        rocketPlugins.Start();
    }

    public void Update()
    {
        rocketPlugins.Update();
    }
}
