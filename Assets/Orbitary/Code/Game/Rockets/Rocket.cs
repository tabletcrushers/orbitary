﻿using UnityEngine;

public class Rocket : MonoBehaviour
{
    public RocketSettings settings;

    void Update()
    {
        float a = Mathf.PI / 2;
        float v = settings.velocity;
        gameObject.transform.Translate(new Vector3(v * Time.deltaTime, 0));
    }
}
